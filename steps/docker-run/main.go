package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
)

const dockerHostDefault = "/var/run/docker.sock"

var (
	image  = flag.String("image", "", "")
	steps  = flag.String("steps", "", "")
	jobDir = flag.String("job-dir", "", "")
)

func main() {
	flag.Parse()
	if *image == "" {
		panic("--image is required")
	}
	if *steps == "" {
		panic("--steps is required")
	}
	if *jobDir == "" {
		panic("--job-dir is required")
	}
	dockerHost := os.Getenv("DOCKER_HOST")
	args := []string{
		"run",
		"-e", "STEPS=" + *steps,
	}
	// I have to use Go because I can't do this in exec.command ... hmmmm
	if dockerHost != "" {
		args = append(args, "-e", "DOCKER_HOST="+dockerHost)
	}
	args = append(args,
		"-i",
		"-v", *jobDir+":"+*jobDir,
	)
	if dockerHost == "" {
		args = append(args, "-v", dockerHostDefault+":"+dockerHostDefault)
	}
	args = append(args,
		"-w", *jobDir,
		*image,
		"/step-runner", "ci",
	)
	cmd := exec.Command("docker", args...)
	out, err := cmd.CombinedOutput()
	if err != nil || cmd.ProcessState.ExitCode() != 0 {
		fmt.Printf("error running docker: %v: %v", string(out), err)
		os.Exit(cmd.ProcessState.ExitCode())
	}
}
